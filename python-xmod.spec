%global _empty_manifest_terminate_build 0
Name:		python-xmod
Version:	1.3.2
Release:	1
Summary:	Create and recursively fill a temporary directory
License:	MIT
URL:		https://github.com/rec/xmod
Source0:	https://files.pythonhosted.org/packages/87/b8/1a0e180623c12e3592d880340d76b2c2fd16dc8bd2084da8e34b377d973c/xmod-1.3.2.tar.gz
BuildArch:	noarch

%description
``xmod`` is a tiny library that extends a module to do things that normally
only a class could do - handy for modules that "just do one thing".

%package -n python3-xmod
Summary:	Create and recursively fill a temporary directory
Provides:	python-xmod
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-xmod
``xmod`` is a tiny library that extends a module to do things that normally
only a class could do - handy for modules that "just do one thing".

%package help
Summary:	Development documents and examples for xmod
Provides:	python3-xmod-doc
%description help
``xmod`` is a tiny library that extends a module to do things that normally
only a class could do - handy for modules that "just do one thing".

%prep
%autosetup -n xmod-1.3.2
sed -i '/dek/d' requirements.txt
sed -i '/dek/d' xmod.egg-info/requires.txt

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-xmod -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Aug 5 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 1.3.2-1
- Update to 1.3.2

* Tue Mar 1 2022 zhangy <zhangy1317@foxmai.com>
- Fix install require

* Thu Dec 31 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
